<?php


namespace App\BookTitle;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;


class BookTitle extends DB
{

    public $id="";

    public $book_title="";

    public $author_name="";


    public function __construct(){

        parent::__construct();

    }

    public function index(){
        echo "BookTitle found!";
    }
    public function setData($postVariabledata=NULL){

        if(array_key_exists('id',$postVariabledata)){
            $this->id = $postVariabledata['id'];
        }

        if(array_key_exists('book_title',$postVariabledata)){
            $this->book_title = $postVariabledata['book_title'];
        }

        if(array_key_exists('author_name',$postVariabledata)){
            $this->author_name = $postVariabledata['author_name'];
        }
    }

    public function store(){

        $arrData = array($this->book_title, $this->author_name);

        $sql = "Insert INTO book_title(book_title,author) VALUES(?,?)";


        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully! :)");
        else
            Message::message("Failed! Data Has Not Been Inserted! :(");

        Utility::redirect('create.php');

    } //end of store method

}

